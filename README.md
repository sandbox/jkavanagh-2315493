CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------
Provides a hierarchical (tree) view of a nested taxonomy

REQUIREMENTS
------------
This module requires the following modules:


Recommended modules
-------------------


INSTALLATION
------------
 * Install as you would normally install a contributed drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------


TROUBLESHOOTING
---------------

FAQ
---


MAINTAINERS
-----------
Current maintainers:
 * Jim Kavanagh - https://www.drupal.org/user/2509462
